//A1.5: AB "Ausgabeformatierung 1" Aufgabe 1
public class ConsolePrint {

	public static void main(String[] args) {

		System.out.print("Hallo wie geht es?\n");
		System.out.print("Mir geht es gut und dir?\n\n");
		System.out.println("Hallo wie geht es? ");
		System.out.println("Er sagt \"Mir geht es gut und dir?\"\n\n");
		System.out.println("Hallo wie geht es? ");
		System.out.println("Mir geht es gut und dir?\n");
		System.out.print(" __    __ _                   _     _                     _ _        ___ \n"
				+ "/ / /\\ \\ (_) ___    __ _  ___| |__ | |_    ___  ___    __| (_)_ __  / _ \\\n"
				+ "\\ \\/  \\/ / |/ _ \\  / _` |/ _ \\ '_ \\| __|  / _ \\/ __|  / _` | | '__| \\// /\n"
				+ " \\  /\\  /| |  __/ | (_| |  __/ | | | |_  |  __/\\__ \\ | (_| | | |      \\/ \n"
				+ "  \\/  \\/ |_|\\___|  \\__, |\\___|_| |_|\\__|  \\___||___/  \\__,_|_|_|      () \n"
				+ "                   |___/                                                 \n\n");
		int alter = 16;
		String name = "Simon";
		System.out.print("Ich hei�e " + name + " und bin " + alter + " Jahre alt.");
		
	}

}

//Der Unterschied zwischen "print()-" und "println()-" ist das bei "brintln()-" automatisch in eine neue Ziel umgebrochen wird.
