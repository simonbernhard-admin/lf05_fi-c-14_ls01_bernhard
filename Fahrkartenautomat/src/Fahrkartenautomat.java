﻿import java.util.InputMismatchException;
import java.util.Scanner;

class Fahrkartenautomat
{	
	private static Scanner tastatur = new Scanner(System.in);
	private static int beenden = 1;
	
	public static int userEingabe(){
		
		int userEingabe = -1;		
		try{
			userEingabe = tastatur.nextInt();			
		}
		catch(InputMismatchException e) {
			System.out.println("Eingabe ungültig! Bitte wiederholen.");
			tastatur.nextLine();
		}		
		return userEingabe;
	}
	
	public static void ticketmenü(String[] ticket, double[] preise) {
		System.out.print("Fahrkartenbestellvorgang:");
		System.out.println();
		for(int i = 0; i < 25; i++) {
			System.out.print("=");
		}
		for(int i = 0; i < 2; i++) {
			System.out.println();
		}
		System.out.println("Wählen Sie ihre Wunschfahrkarte für Berlin AB aus:");
		for(int i = 0; i < ticket.length; i++) {
			String preiseAlsstring = String.format("%.2f", preise[i]);
			System.out.println("	" + ticket[i] + " [" + preiseAlsstring + " EUR] (" + (i+1) + ")");
		}		
		System.out.println("	Bezahlen (99)");
		System.out.println();
	}

	public static double fahrkartenbestellungErfassen() 
	{
		double preis = 0;
		int anzahl = 0;
		boolean preisPrüfung = false;
		boolean anzahlPrüfung = false;
		int ticketEingabe = -2;
		int anzahlEingabe = -2;
		double summe = 0.0;
		String[] ticket = {"Einzelfahrschein Berlin AB", "Einzelfahrschein Berlin BC", "Einzelfahrschein Berlin ABC",
							"Kurzstrecke", "Tageskarte Berlin AB", "Tageskarte Berlin BC", "Tageskarte Berlin ABC",
							"Kleingruppen-Tageskarte Berlin AB", "Kleingruppen-Tageskarte Berlin BC",
							"Kleingruppen-Tageskarte Berlin ABC"};
		double[] preise = {2.9, 3.3, 3.6, 1.9, 8.6, 9, 9.6, 23.5, 24.3, 24.9};
		
		

		do {
			preis = 0;
			anzahl = 0;
			preisPrüfung = false;
			anzahlPrüfung = false;
			ticketEingabe = -2;
			anzahlEingabe = -2;			
			ticketmenü(ticket, preise);

			do {		
				System.out.print("Ihre Wahl:");			

				ticketEingabe = userEingabe();			
				while(ticketEingabe == -1) {
					System.out.print("Ihre Wahl:");
					ticketEingabe = userEingabe();				
				}
				if(ticketEingabe > 0 && ticketEingabe <= ticket.length || 
				   ticketEingabe == 99) {
					
					if(ticketEingabe == 99) {
						if(summe > 0) {
							preisPrüfung = true;
						}
						else {
							System.out.println("Bitte wählen Sie zuerst ein Ticket aus!");
						}
					}					
					else {
						preis = preise[(ticketEingabe-1)];
						preisPrüfung = true;
					}
				}
				else {
					System.out.println("Falsche Eingabe! Bitte wiederholen Sie Ihre Eingabe!");
				}
			}while(!preisPrüfung);						

			while(!anzahlPrüfung && ticketEingabe != 99){
							

				System.out.print("Anzahl der ticket:");
				anzahlEingabe = userEingabe();	

				while(anzahlEingabe == -1) {
					System.out.print("Anzahl der ticket:");
					System.out.println(anzahlEingabe);
					anzahlEingabe = userEingabe();				
				}			
				if(anzahlEingabe >= 0) {
					anzahl = anzahlEingabe;
					anzahlPrüfung = true;
				}
				else {
					System.out.println("Falsche Eingabe! Bitte wiederholen Sie Ihre Eingabe!");
				}
			}
			summe += anzahl * preis;
			if(ticketEingabe != 99) {
				String summeAlsString = String.format("%.2f", summe);
				System.out.println();
				System.out.println("summe: " + summeAlsString + " €");
				System.out.println();
			}			
		}while(ticketEingabe != 99);


		double zuZahlenderBetrag = summe;
		return zuZahlenderBetrag;	    
	}

	public static double fahrkartenBezahlen(double zuZahlenderBetrag) 
	{		
		double eingezahlterGesamtbetrag = 0.00;
		double nochZuZahlenderBetrag =  zuZahlenderBetrag - eingezahlterGesamtbetrag;	       
		double eingeworfeneMünze;

		while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
		{    
			String nochZuZahlenderBetragAlsString = String.format("%.2f", nochZuZahlenderBetrag);
			System.out.println("Noch zu zahlen: " + nochZuZahlenderBetragAlsString + " Euro");
			System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
			eingeworfeneMünze = tastatur.nextDouble();
			if(eingeworfeneMünze == 0.05 || eingeworfeneMünze == 0.1 || eingeworfeneMünze == 0.2 ||
					eingeworfeneMünze == 0.5 || eingeworfeneMünze == 1 || eingeworfeneMünze == 2) {
				eingezahlterGesamtbetrag += eingeworfeneMünze;
				nochZuZahlenderBetrag = zuZahlenderBetrag - eingezahlterGesamtbetrag;
			}
			else {
				System.out.println("Eingabe ist ungültig! Bitte versuchen Sie es mit einer anderen Münze nochmal!");
			}

		}		
		return eingezahlterGesamtbetrag-zuZahlenderBetrag;
	}

	public static void warte(int millisekunden) 
	{
		try {
			Thread.sleep(millisekunden);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void münzeAusgeben(int betrag, String einheit) 
	{
		System.out.println(betrag + " " + einheit);
	}

	public static void fahrkartenAusgeben()
	{
		System.out.println("\nFahrschein wird ausgegeben");
		for (int i = 0; i < 8; i++)
		{
			System.out.print("=");
			warte(250);
		}
		System.out.println("\n\n");
	}

	public static void rueckgeldAusgeben(double rückgabebetrag) 
	{
		if(rückgabebetrag > 0.0)
		{
			String rückgabebetragAlsString = String.format("%.2f", rückgabebetrag);
			System.out.println("Der Rückgabebetrag in Höhe von " + rückgabebetragAlsString + " EURO");
			System.out.println("wird in folgenden Münzen ausgezahlt:");

			while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
			{				
				münzeAusgeben(2, "EURO");
				rückgabebetrag -= 2.0;
			}
			while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
			{				
				münzeAusgeben(1, "EURO");
				rückgabebetrag -= 1.0;
			}
			while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
			{				
				münzeAusgeben(50, "CENT");
				rückgabebetrag -= 0.5;
			}
			while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
			{				
				münzeAusgeben(20, "CENT");
				rückgabebetrag -= 0.2;
			}
			while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
			{				
				münzeAusgeben(10, "CENT");
				rückgabebetrag -= 0.1;
			}
			while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
			{				
				münzeAusgeben(5, "CENT");
				rückgabebetrag -= 0.05;
			}
		}

		System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
				"vor Fahrtantritt entwerten zu lassen!\n"+
				"Wir wünschen Ihnen eine gute Fahrt.");
		System.out.println();
	}

	public static void main(String[] args)
	{
		while(beenden == 1) {
			double zuZahlenderBetrag = fahrkartenbestellungErfassen();
			double rückgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);
			fahrkartenAusgeben();
			rueckgeldAusgeben(rückgabebetrag);

		}	
		tastatur.close();
	}    
}