
public class Mittelwert {

   public static void main(String[] args) {

      // (E) "Eingabe"
      // Werte f�r x und y festlegen:
      // ===========================
      double x = 2.0;
      double y = 4.0;
      double m = 0.0;
      
      // (V) Verarbeitung
      // Mittelwert von x und y berechnen: 
      // ================================
      m = berechneMittelwert(x, y); // x und y werden in berechne Mittelwert importiert (2.0 = x = a, 4.0 = y = b, m = o)
      
      // (A) Ausgabe
      // Ergebnis auf der Konsole ausgeben:
      // =================================
      System.out.printf("Der Mittelwert von %.2f und %.2f ist %.2f\n", x, y, m);
   }

   public static double berechneMittelwert(double a, double b) { // Datentyp immer hinter Name. In Klammer Import Daten.
	// TODO Auto-generated method stub
	double o = (a + b) / 2.0;
	return o; // Ausgabe zur�ck zu m = berechneMittelwert(x, y);
	
}
}
